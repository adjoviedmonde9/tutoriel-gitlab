Installation python sur vos ordinateurs de bureau
==============

Dans le cas d'une connaissance limitée de l'environnement python nous vous conseillons de passer par 
la distribution anaconda. 

Anaconda
----------

Anaconda est une distribution, elle permet d'avoir un environnement de travail autour du projet 
python. 
Un ensemble de packages est déjà installé et l'outil conda d'installation de package python est 
a 
disposition pour des packages supplémentaires. Cela permet d'avoir rapidement un 
environnement de travail fonctionnel avec l'essentiel des packages utiles.

Pour l'installation nous vous conseillons de suivre ce lien (en anglais) et d'installer la version 3.6 de 
python: [Installation d'Anaconda 
sous 
Windows](https://docs.anaconda.com/anaconda/install/windows.html)

Installation
-------------

Une fois sur cette [page](https://www.anaconda.com/download/#windows) télécharger la 
version Python 3.6 

![1_anaconda](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/1_anaconda.PNG)

Double cliquer sur le lancer d'installation que vous venez de télécharger au format 
Anaconda3-X.X.X-Windows-XX_XX.exe qui devrait se trouver dans votre dossier de 
téléchargement.

![2_executer](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/2_executer.PNG)

Executer.

![3_next](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/3_next.PNG)

Cliquer sur "Next".

![4_i_agree](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/4_i_agree.PNG)

Accepter les termes de la license.

![5_just_me](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/5_just_me.PNG)

Indiquer "Just me" puis "Next".

![6_le_bon_chemin](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/6_le_bon_chemin.PNG)

De préférence installer anaconda dans 
```C:\Users\votre.nom\AppData\Local\Continuum\anaconda3``` ou tout autre 
dossier 
vous appartenant ainsi 
les 
droits administrateurs ne 
seront en théorie pas nécessaires et l'installation de nouveaux packages sera 
facilitée.

![7_install](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/7_install.PNG)

Après avoir cliqué sur la deuxième boîte "Register Anaconda..." cliquer sur 
"Install".

![8_next](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/8_next.PNG)

Cliquer sur suivant.

![9_pas_vscode](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/9_pas_vscode.PNG)

NE PAS INSTALLER Microsoft VSCode.

![10_congrats](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/10_congrats.PNG)

Installation réussie.

Anaconda devrait se trouver sur votre bureau et dans la barre de démarage Windows. 
Nous conseillons l'utilisation de Jupyter directement: dans votre barre de démarage 
chercher "Jupyter Notebook"

Ajout d'Anaconda dans le PATH
-----------------------------

Cette étape est conseillée et nécessaire pour l'ajout de nouvelles bibliothèques dans votre environnement 
python.

L'idée est la suivante: On va permettre à votre invite de commande de retrouvé l'ensemble des 
fonctionnalités annaconda que l'on vient d'ajouter. Notamment ```pip``` et ```conda``` qui vont permettre 
d'installer de nouvelles bibliothèques. Pour se faire on va ajouter ou modifier le ```PATH``` de votre 
compte. Le ```PATH``` recense tous les chemins dans lesquels fouiller pour trouver des commandes. 
Il y en a un global 
pour votre système et un autre particulier lié à votre compte. Ce dernier peut être modifié sans droits 
administrateurs.

![11_menu_demarrer](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/11_menu_demarrer.PNG)

Dans "Menu Démarrer" taper "variable environnement" et cliquer sur "Modifier les variables 
d'environnement pour votre compte.

![12_var_env](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/12_var_env.PNG)

Ensuite deux cas se présentent:
- Soit la variable PATH est déjà présente
- Soit la variable PATH n'est pas présente

Dans le cas où il n'y a pas de variable PATH on va simplement l'ajouter. Cliquer sur le bouton 
"Nouvelle...".

![13_nouvelle](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/13_nouvelle.PNG)

Ajouter la variable ```PATH``` comme nom et "le chemin où Anaconda est 
installé" + ```\Scripts```. 
Normalement la valeur doit donc être: ```C:\Users\votre.nom\AppData\Local\Continuum\anaconda3\Scripts``` . 
Valider en cliquant sur OK.

Dans le cas où la variable ```PATH``` existe déjà. Cliquer sur 
"Modifier...".

![14_modifier](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/14_modifier.PNG)

NE PAS REMPLACER L'EXISTANT. Il faut ajouter le chemin des scripts Anaconda **à la fin** en séparant par un 
```;```. En théorie ```;C:\Users\votre.nom\AppData\Local\Continuum\anaconda3\Scripts``` à la fin.

![15_modifier_fin](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/15_modifier_fin.PNG)

Pour finir on vérifie que le nouveau ```PATH``` est pris en compte.

![16_cmd](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/16_cmd.PNG)

Chercher l'invite de commande en tapant "cmd" dans le "Menu Démarrer". __Attention__: pour^que la variable 
```PATH``` soit prise en considération par l'invite de commande, il faut ouvrir ou réouvrir l'invite de 
commande __après__ avoir modifié la variable ```PATH```.

![17_conda](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/17_conda.PNG)

Taper ensuite ```conda``` dans l'invite de commande.

![18_conda_ok](https://gitlab.com/DREES/tutoriels/raw/master/img/anaconda/18_conda_ok.PNG)

Si tout s'est bien passé l'explication de la commande ```conda``` devrait s'afficher.

L'installation est désormais terminée. Félicitations!
